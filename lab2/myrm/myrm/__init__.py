#usr/lib/env python2.7
#-*- coding: utf-8 -*-

"""
Utility to remove files and directories with support for the Trash


To install, run the command:
	
	python setup.py install

To use the package in the intepriter, import the required modules:

	argparser - work with arguments
	deleter   - class for work with functions of remove
	trash     - class for work with functions of move to Trash
	modes     - politic's decorators of some deleter and trash functions
	logger    - class for work with logging
	file      - class for work with creation substance File
	config    - functions for work with configurations of argument by file 
	errors    - class for work with exceptions
"""
__version__ = "3.0"
