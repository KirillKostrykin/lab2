#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import argparse

def create_parser():
	"""Handling arguments"""
	myrm_parser = argparse.ArgumentParser()

	myrm_parser.add_argument('--clear', action='store_true', help="clear Trash")

	myrm_parser.add_argument('--removelist', action='store_true', help="show list of files and directories in Trash")

	myrm_parser.add_argument('-d', '--dir', action='store_true', help='remove empty directories')
	myrm_parser.add_argument('-r', '-R', '--recursive', action='store_true', help='remove directories and their contents recursively')
	myrm_parser.add_argument('path', nargs='*', help='list of paths after flag')
	myrm_parser.add_argument('-t', '--totrash', action='store_true', help='remove files and directories to Trash')

	myrm_parser.add_argument('--restore', action='store_true', help="restore files and directories out of Trash by inode")
	myrm_parser.add_argument('--filepath', action='store_true', help="restore files and directories out of Trash by file path")

	myrm_parser.add_argument('--config', default='config_file.json', help="installation of programm settings from JSON file")

	myrm_parser.add_argument('--politics',  action='store_true', help="turn on max_time and max_size politics")

	myrm_parser.add_argument('--dry_run',  action='store_true', help="turn on dry-run mode")

	myrm_parser.add_argument('--max_file_time', type=int, help="override the default setting of max time file saved in Trash")
	myrm_parser.add_argument('--max_file_size', type=int, help="override the default setting of max size file saved in Trash")
	myrm_parser.add_argument('--trash_path', help="override the default setting of Trash path")
	
	myrm_parser.add_argument('-f', '--force', action='store_true', help="turn on force mode(have no questions when remove)")
	myrm_parser.add_argument('--silent', action='store_true', help="turn on silent mode(have no stdout)")

	myrm_parser.add_argument('--regex', help="remove all files, which matches to a regular expression")
	
	args = myrm_parser.parse_args()

	return args