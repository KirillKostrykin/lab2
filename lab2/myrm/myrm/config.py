#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

import json
import os.path
import myrm.errors as errors

DEFAULT_TRASH_PATH = "~/Trash"
DEFAULT_MAX_TRASH_SIZE = 1000000000000
DEFAULT_MAX_FILE_SIZE = 205000000
DEFAULT_MAX_FILE_TIME = 600

def make_politics_dict(file_path):
	"""Function, that parse file with file_path by JSON parser and return dict of arguments.
	In case of file not exists, create file with default config and parse it
	"""
	if file_path is None:
		file_path = "config_file.json"

	file_path = os.path.abspath(file_path)
	default_config = get_default_config_json()

	if not os.path.exists(file_path):
		with open(file_path, 'w') as f:
			f.write(default_config)
			result = json.load(f)
	else:
		with open(file_path) as f:
			result = json.load(f)

	return result

def get_default_config_json():

	return 	'''
{
	"trash_path": "%s",
	"max_trash_size": %s,
	"max_file_size": %s,
	"max_file_time": %s
}
''' % (
	DEFAULT_TRASH_PATH,
	DEFAULT_MAX_TRASH_SIZE,
	DEFAULT_MAX_FILE_SIZE,
	DEFAULT_MAX_FILE_TIME
)

def merge_config_params_with_args(config_dict, arg_dict):
	"""Function, that update first dict with such params in second"""
	try:
		result = {}
		for elem in config_dict:
			if elem in arg_dict:
				result[elem] = arg_dict[elem]
			else:
				result[elem] = config_dict[elem]

		return result

	except LookupError as err:
		raise errors.LookupError(msg=err.strerror)