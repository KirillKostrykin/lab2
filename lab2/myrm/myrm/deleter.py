#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
"""Class Deleter to create a substance, which contains of given flags

Field(-s) of this class object:
empty_dir - bool of appropriate flag
recursive - bool of appropriate flag
totrash - bool of appropriate flag
logger - appropriate object of class Logger

Available method:
remove_file(file_path) - method to remove files
remove_dir(dir_path)- method to remove empty folders
remove_not_empty_dir(dir_path) - recursive method to remove folders
with content(at first the attached files then attached folders)
remove(path) - generalized method using the above-mentioned methods to
remove any kind of files and folders
"""
import os
import logging
from logging import INFO, WARNING, ERROR, CRITICAL, DEBUG
import myrm.errors as errors
import myrm.logger as logger
import myrm.modes as modes

class Deleter(object):
	"""Class for work with paths of removal files and directories

	Keyword arguments:
	empty_dir         - remove directories
	recursive         - remove directories and their contents recursively
	totrash           - to remove to Trash
	dry_run           - if set, imitate what would have happened, no changes are required
	force             - if set, ignore non-existent files and arguments, never prompt,
	                    show only result of implementation
	silent            - if set, never prompt, status only in exit code
	regex             - regular expression
	logger            - logger object
	"""
	def __init__(self, recursive=False,
				 empty_dir=False,
				 dry_run=False,
				 force=False,
				 silent=False,
				 totrash=False,
				 regex=False,
				 logger_=logger.Logger(DEBUG)
				):
		self.recursive = recursive
		self.empty_dir = empty_dir
		self.dry_run = dry_run
		self.force = force
		self.silent = silent
		self.totrash = totrash
		self.regex = regex
		self.logger = logger_

	@modes.regular_expression
	@modes.interactive
	def remove_file(self, path):
		"""Remove file by path

		Return list of success removed files
		If except OSError, FileRemoveError will be raised
		"""
		success_removed_files = []
		try:
			if not self.dry_run:
				os.remove(path)
				success_removed_files.append(os.path.abspath(path))

			if self.dry_run:
				self.logger.log(INFO, "{} file was deleted".format(path))
		
		except OSError as err:
			raise errors.FileRemoveError(msg=err.strerror)

		return success_removed_files
	
	@modes.interactive
	def remove_dir(self, path):
		"""Remove empty folder by path

		Return list of success removed files.
		If flag 'dir' or 'recursive' not send, IsDirectoryError will be raised
		"""
		success_removed_files = []

		if os.path.isdir(path):
			if not self.empty_dir and not self.recursive:
				self.logger.log(WARNING, "cannot remove {}. It's a directory".format(path))
				raise errors.IsDirectoryError(msg="Expected file, but it's a directory")

		if not self.dry_run:
			os.rmdir(path)
			success_removed_files.append(os.path.abspath(path))
		
		if self.dry_run:		
			self.logger.log(INFO, "{} folder was deleted".format(path))

		return success_removed_files

	@modes.interactive
	def remove_link(self, path):
		"""Remove link

		Return list of success removed files
		"""
		success_removed_files = []

		if not self.dry_run:
			os.unlink(path)
			success_removed_files.append(os.path.abspath(path))

		if self.dry_run:
			self.logger.log(INFO, "{} link was unlinked".format(path))

		return success_removed_files


	def remove_not_empty_dir(self, path, delself=True):
		"""Remove not empty directory by path

		Return list of success removed files

		If send directory path and flags 'recursive' and 'dir' are False, IsDirectoryError will be raised
		If directory not empty and flag 'recursive' is False, NotEmptyDirectoryError will be raised
		"""
		success_removed_files = []

		if os.path.isdir(path):
			if not self.empty_dir and not self.recursive:
				self.logger.log(WARNING, "cannot remove {}. It's a directory".format(path))
				raise errors.IsDirectoryError(msg="Expected file, but it's a directory")

		listdir = os.listdir(path)

		if len(listdir) > 0 and not self.recursive:
			self.logger.log(WARNING, "cannot remove {}. Directory not empty".format(path))
			raise errors.NotEmptyDirectoryError(msg="Directory not empty")			

		for elem in listdir:
			full_path = os.path.join(path, elem)
		
			if os.path.isfile(full_path):
				success_removed_files.extend(self.remove_file(full_path))
		
			if os.path.isdir(full_path):
				#for elem in self.remove_not_empty_dir(full_path):
				success_removed_files.extend(self.remove_not_empty_dir(full_path))

			if os.path.islink(path):
				success_removed_files.extend(self.remove_link(path))

		if delself:
			success_removed_files.extend(self.remove_dir(path))

		return success_removed_files

	def remove(self, path):
		"""Remove anyone files and folders by path

		Return list of success removed files
		"""
		if not os.path.exists(path):
			self.logger.log(WARNING, "cannot remove {}. No such file or directory".format(path))
			raise errors.NotFoundError(msg="No such file or directory")
		
		if os.path.islink(path):
			return self.remove_link(path)

		if os.path.isfile(path):
			return self.remove_file(path)
	
		if os.path.isdir(path):
			return self.remove_not_empty_dir(path)
