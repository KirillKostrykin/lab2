#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

"""Class Error to create our own type of exception with such fields as:
errno - exit code
msg - information message
"""
class RMError(Exception):
	errno = 111
	def __init__(self, msg):
		self.msg = msg

class FileAccessError(RMError):
	errno = 2

class NotFoundError(RMError):
	errno = 3

class IndexError(RMError):
	errno = 4

class IsDirectoryError(RMError):
	errno = 5

class NotADirectoryError(RMError):
	errno = 6
	
class FileExistsError(RMError):
	errno = 7

class FileRemoveError(RMError):
	errno = 8

class FileWorkingError(RMError):
	errno = 9

class LookupError(RMError):
	errno = 10

class NotEmptyDirectoryError(RMError):
	errno = 11