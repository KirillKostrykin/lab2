#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

"""Class File to create a substance with such fields as:
inode - create in time of initialization
path - create in time of initialization
"""
import ConfigParser
import os
import time

class File(object):
	"""Object for work with files

	Keyword arguments:
	inode             - file inode
	path              - file path
	change_time       - time of the latest change of file
	removal_time      - time of file removal
	"""
	def __init__(self, path):
		self.inode = os.stat(path).st_ino
		self.path = os.path.abspath(path)
		self.change_time = os.path.getmtime(path)
		self.removal_time = time.time()