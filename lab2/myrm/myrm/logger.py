#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

"""Class Logger to create a substance to logging"""
import logging

class Logger(object):
	def __init__(self, level_):
		logging.basicConfig(
			format='%(asctime)s - %(levelname)s - %(message)s',
			level=level_,
			)

		self.logger = logging.getLogger()
		
	def log(self, level ,message):
		"""Method to logs the message according to the level"""
		self.logger.log(level, message)