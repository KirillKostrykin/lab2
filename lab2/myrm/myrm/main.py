#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import myrm.argparser as argparser
import myrm.deleter as deleter
import myrm.trash as trash
import myrm.logger as logger
import myrm.config as config
import json
import myrm.errors as errors

from logging import INFO, WARNING, ERROR, CRITICAL, DEBUG

def main():
	exit_code = 0
	
	args = vars(argparser.create_parser())

	my_config = config.make_politics_dict(args['config'])

	if not  args.get('max_file_time'):
		args['max_file_time'] = int(my_config['max_file_time'])
	if not args.get('max_file_size'):
		args['max_file_size'] = int(my_config['max_file_size'])
	if not args.get('trash_path'):
		args['trash_path'] =  os.path.expanduser(my_config['trash_path'])

	if args['silent']:
		my_logger = logger.Logger(CRITICAL)
	else:
		my_logger = logger.Logger(DEBUG)
	my_deleter = deleter.Deleter(
		recursive=args['recursive'],
		empty_dir=args['dir'],
		dry_run=args['dry_run'],
		force=args['force'],
		silent=args['silent'],
		totrash=args['totrash'],
		regex=args['regex'],
		logger_=my_logger
		)

	trasher = trash.Trash(
		max_file_time=args['max_file_time'], 
		max_file_size=args['max_file_size'],
		trash_path=args['trash_path'],
		recursive=args['recursive'],
		empty_dir=args['dir'],
		dry_run=args['dry_run'],
		force=args['force'],
		silent=args['silent'],
		regex=args['regex'],
		totrash=args['totrash'],
		logger_=my_logger) 

	
	
	success_removed_files = []

	if args['removelist']:
		try:
			trasher.get_trash_elems_info()
		except errors.RMError as err:
			exit_code = err.errno

	elif args['clear']:
		trasher.clear(my_deleter)

	elif args['restore'] and args['filepath']:
		for elem in args['path']:
			trasher.restore_file_with_path(elem)
		
	elif args['restore']:
		for elem in args['path']:
			trasher.restore_file_with_inode(elem)
	
	elif my_deleter.totrash:
		for elem in args['path']:
			try:	
				if os.path.isdir(elem):
					if not my_deleter.empty_dir and not my_deleter.recursive:
						my_deleter.logger.log(WARNING, "cannot remove {}. It's a directory".format(elem))
						raise errors.IsDirectoryError(msg="Expected file, but it's a directory")

					listdir = os.listdir(elem)

					if len(listdir) > 0 and not my_deleter.recursive:
						my_deleter.logger.log(WARNING, "cannot remove {}. Directory not empty".format(elem))
						raise errors.NotEmptyDirectoryError(msg="Directory not empty")	

				trasher.move_to_trash(elem)

			except errors.RMError as err:
				exit_code = err.errno

		if args['politics']:
			trasher.check_politics(my_deleter)
		
	elif args['politics']:
		trasher.check_politics(my_deleter)
	
	else:
		for elem in args['path']:
			try:
				success_removed_files.extend(my_deleter.remove(elem))
		
			except errors.RMError as err:
				exit_code = err.errno
		
		for elem in success_removed_files:
			my_deleter.logger.log(INFO, "{} file was removed".format(elem))

		exit(exit_code)

if __name__ == '__main__':
	main()
