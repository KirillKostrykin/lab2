#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import os.path
import myrm.errors as errors
import re

from logging import WARNING

def interactive(func):
	def wrapper(self, path):
		if not os.path.exists(path):
			self.logger.log(WARNING, "cannot remove {0}. No such file or directory".format(path))
			raise errors.NotFoundError(msg="No such file or directory")
			
		if not self.silent and not self.force:
			
			if self.totrash:
				if os.path.islink(path):
					print 'Do you want to move link "{}" to Trash?'.format(path)
				elif os.path.isfile(path):
					print 'Do you want to move file "{}" to Trash?'.format(path)
				elif os.path.isdir(path):
					print 'Do you want to move directory "{}" to Trash?'.format(path)
			else:
				if os.path.islink(path):
					print 'Do you want to remove link "{}"?'.format(path)
				elif os.path.isfile(path):
					print 'Do you want to remove file "{}"?'.format(path)
				elif os.path.isdir(path):
					print 'Do you want to remove directory "{}"?'.format(path)

			ans = raw_input()

			if ans == 'y' or ans == 'Y':
				return func(self, path)
		else:
			return func(self, path)

	return wrapper

def trash_exceptions(func):
	def wrapper(self):
		if not os.path.exists(self.trash_path):
			self.logger.log(WARNING, "Trash is not found")
			raise errors.NotFoundError(msg="Trash is not found")

		if not os.path.exists(self.infodir_path) or not os.path.exists(self.filedir_path):
			self.logger.log(WARNING, "You can not work with Trash. Trash is damaged")
			raise errors.NotFoundError(msg="Trash is damaged")	
		
		return func(self)

	return wrapper

def regular_expression(func):
	def wrapper(self, path):
		if not os.path.exists(path):
			self.logger.log(WARNING, 'cannot remove "{}". No such file or directory'.format(path))

			raise errors.NotFoundError(msg="No such file or directory")

		if self.regex:
			regex = self.regex
			file_basename = os.path.basename(path)
			print re.search(regex, file_basename), 'file', file_basename, regex

			if re.search(regex, file_basename) != None:
				print re.search(regex, file_basename).group(0), 'file', file_basename, regex
				
				if re.search(regex, file_basename).group(0) == file_basename:
					return func(self, path)
		else:
			return func(self, path)


	return wrapper