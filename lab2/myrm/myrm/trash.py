#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

"""Class Trash to create a directory, which will be used as a LINUX Trash
Folder (default name)'Trash' contains 2 folders: 'files' and 'info'.
In first folder arevremoval files or directories with names of there
inode, in the second - files with such names with info about removal
objects.

Field(-s) of this class object:
trash_path(default = 'Trash') - signify a path to you Trash

Available method:
move_to_trash(file_path) - move file with file_path to 'Trash/files'
add_file_info(file) - add infofile in 'Trash/info' by the file.inode
get_trash_elems() - return list of elems, which are #tained in Trash now 
restore_file_with_inode(file_inode) - return bool, which signify the
result of attempt to restore file by inode
"""
import os
import shutil
import pickle
import myrm.file as file
import myrm.logger as logger
from logging import INFO, WARNING, ERROR, CRITICAL, DEBUG

import myrm.config as config
import time
import myrm.errors as errors
import myrm.modes as modes

class Trash(object):
	"""Class for work with Trash

	Keyword arguments:
 	max_file_time     - maximal file time politic for a one session
	max_file_size     - maximal file size politic for a one session
	trash_path        - path to the Trash for a one session
	empty_dir         - remove directories
	recursive         - remove directories and their contents recursively
	totrash           - to remove to Trash
	dry_run           - if set, imitate what would have happened, no changes are required
	force             - if set, ignore non-existent files and arguments, never prompt,
	                    show only result of implementation
	silent            - if set, never prompt, status only in exit code
	logger            - logger object
	"""
	TABLE_TITLE = """\n{:*<71}\ninode   path{: <32}\n{:*<71}""".format('','','')

	def __init__(self, 
				 max_file_time=config.DEFAULT_MAX_FILE_TIME, 
				 max_file_size=config.DEFAULT_MAX_FILE_SIZE,
				 trash_path=os.path.expanduser(config.DEFAULT_TRASH_PATH),
				 recursive=False,
				 empty_dir=False,
				 dry_run=False,
				 force=False,
				 silent=False,
				 totrash=False,
				 regex=False,
				 logger_=logger.Logger(DEBUG)
				):
		self.max_file_time = max_file_time
		self.max_file_size = max_file_size
		self.trash_path = trash_path
		self.filedir_path = os.path.join(self.trash_path, 'files/')
		self.infodir_path = os.path.join(self.trash_path, 'info/')
		self.recursive = recursive
		self.empty_dir = empty_dir
		self.dry_run = dry_run
		self.force = force
		self.silent = silent
		self.totrash = totrash
		self.regex = regex
		self.logger = logger_

	def add_file_info(self, file_):
		"""Method to add infofile in folder 'info' in Trash by the file.inode"""
		meta_file_path = os.path.join(self.infodir_path, str(file_.inode))

		with open(meta_file_path, 'wb') as f:
			pickle.dump(file_, f)

	def create_trash(self, path):
		"""Method to create directory with path"""
		if not os.path.exists(path):
			os.makedirs(path)
			self.trash_path = os.path.expanduser(path)
			return True
		else:
			return False

	@modes.interactive
	def move_to_trash(self, path):
		"""Method to move file with file_path to folder 'files' in Trash"""
		info = file.File(path)

		if not os.path.exists(self.trash_path):
			os.makedirs(self.trash_path)
		if not os.path.exists(self.infodir_path):
			os.mkdir(self.infodir_path)
		if not os.path.exists(self.filedir_path):
			os.mkdir(self.filedir_path)

		self.add_file_info(info)	

		if os.path.isdir(path):
			if not self.dry_run:
				os.rename(path, os.path.join(self.filedir_path, str(info.inode)))
			self.logger.log(INFO, "{} dir was moved to Trash".format(path))

		if os.path.isfile(path):
			if not self.dry_run:
				os.rename(path, os.path.join(self.filedir_path, str(info.inode)))
			self.logger.log(INFO, "{} file was moved to Trash".format(path))		

	@modes.trash_exceptions
	def get_trash_elems(self):
		"""Function, that return list of elements which are in Trash now"""	
		try:
			trash_file_list = []
						
			for elem in os.listdir(self.infodir_path):

				with open (os.path.join(self.infodir_path, elem),'rb') as f:
					data_new = pickle.load(f)
					trash_file_list.append(data_new)

			return trash_file_list

		except IndexError as err:
			raise errors.IndexError(msg=err.strerror)

		except OSError as err:
			raise errors.FileWorkingError(msg=err.strerror)

	def get_trash_elems_info(self):
		"""Function, that show all information of elements which are in Trash now"""	
		try:
			trash_file_list = self.get_trash_elems()

			if len(trash_file_list) > 0:	
				self.logger.log(INFO, self.TABLE_TITLE)

				for elem in trash_file_list:
					print elem.inode, elem.path

			else:
				self.logger.log(WARNING, "No file in Trash")
		except IOError as err:
			raise errors.NotFoundError(msg=strerror)

	def restore_file_with_inode(self, file_inode):
		"""Restore file with file_inode out of the Trash by search info about it
		in such file in Trash/info.
		In case of file not found, show suitable message
		"""
		try:
			fileinfo_path = os.path.join(self.infodir_path, file_inode)
			file_path = os.path.join(self.filedir_path, file_inode)
			count = 0

			if os.path.exists(fileinfo_path) and os.path.exists(file_path):
				trash_file_list = self.get_trash_elems()
				
				for elem in trash_file_list:
					if str(elem.inode) == file_inode:
				
						if not os.path.exists(os.path.dirname(elem.path)):
							os.makedirs(os.path.dirname(elem.path))
				
						os.rename(os.path.join(self.filedir_path, str(elem.inode)), elem.path)
						self.logger.log(INFO, "File with inode {} restored to {}".format(file_inode, elem.path))
						os.remove(os.path.join(self.infodir_path, str(elem.inode)))					
						count +=1
				
				if count == 0:
					self.logger.log(WARNING, "File with inode {} not found".format(file_inode))
		
		except IndexError as err:
			raise errors.IndexError(msg=err.strerror)
		
		except OSError as err:
			raise errors.FileWorkingError(msg=err.strerror)

	def restore_file_with_path(self, file_path):
		"""Restore file with file_path out of the Trash by search info about it
		in such file in Trash/info.
		In case of file not found, show suitable message
		"""
		count = 0
		trash_file_list = self.get_trash_elems()
		
		for elem in trash_file_list:
			if str(elem.path) == os.path.abspath(file_path):
		
				if not os.path.exists(os.path.dirname(elem.path)):
					os.makedirs(os.path.dirname(elem.path))
		
				os.rename(os.path.join(self.filedir_path, str(elem.inode)), elem.path)
				self.logger.log(INFO, "File with path {} restored to {}".format(file_path, elem.path))
				os.remove(os.path.join(self.infodir_path, str(elem.inode)))	
				count +=1				
		
		if count == 0:
			self.logger.log(WARNING, "File with path {} not found".format(file_path))

	def check_trash_file_time(self):
		"""Function, that return list of file's inodes, which are not correct
		from file-time politic in Trash now
		"""	
		trash_file_list = self.get_trash_elems()
		unsupported_files = []

		for elem in trash_file_list:
			if time.time() - elem.removal_time > self.max_file_time:
				unsupported_files.append(elem.inode)

		return unsupported_files

	def check_trash_file_size(self):
		"""Function, that return list of file's inodes, which are not correct
		from file-size politic in Trash now
		"""
		unsupported_files = []
		
		for elem in os.listdir(self.filedir_path):
		
			if os.path.isfile(os.path.join(self.filedir_path, elem)) and \
				os.path.getsize(os.path.join(self.filedir_path, elem)) > self.max_file_size:
				unsupported_files.append(elem)
		
			if os.path.isdir(elem):
				for elem in os.listdir(elem):
					elem.check_trash_file_size()

		return unsupported_files

	@staticmethod
	def get_size(start_path = '.'):
		"""Function, that return unite size of files in folder"""
		total_size = 0
		
		for dir_path, dir_names, file_names in os.walk(start_path):
		
			for elem in file_names:
				file_path = os.path.join(dir_path, elem)
				total_size += os.path.getsize(file_path)

		return total_size

	def check_trash_size(self):
		"""Function, that return size of file's in Trash"""
		trash_size = 0
		
		for elem in os.listdir(self.filedir_path):
			trash_size += get_size(elem)

		return trash_size

	def flush(self, inode_list, deleter):
		"""Method, that remove files, which inode_list consist of"""
		for elem in inode_list:
			deleter.force = True
			deleter.recursive = True

			deleter.remove(os.path.join(self.filedir_path, str(elem)))
			deleter.remove(os.path.join(self.infodir_path, str(elem)))

	def clear(self, deleter):
		"""Method to clear folders Trash/info and Trash/files"""
		try:	
			deleter.force = True
			deleter.recursive = True

			deleter.remove_not_empty_dir(self.filedir_path, delself=False)
			deleter.remove_not_empty_dir(self.infodir_path, delself=False)

		except IOError as err:
			raise errors.NotFoundError(msg=err.strerror)

		else:
			deleter.logger.log(INFO, "Trash is empty")


	def check_politics(self, deleter):
		"""Method, that log how many unsupported files in Trash by such
		politics and call there remove
		"""
		unsupported_files = self.check_trash_file_time()
		self.logger.log(INFO, str(len(unsupported_files)) + " file(s) will be remove for time poliitic")
		self.flush(unsupported_files, deleter)
		
		unsupported_files = self.check_trash_file_size()
		self.logger.log(INFO, str(len(unsupported_files)) + " file(s) will be remove for size poliitic")
		self.flush(unsupported_files, deleter)
