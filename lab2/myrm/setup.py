#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

from setuptools import find_packages, setup

import myrm
setup(
	name="myrm",
	version=myrm.__version__,
	author="Kirill Kostrykin",
	author_email="kirillkostrykin@gmail.com",
	packages=find_packages(),
	entry_points={
		"console_scripts": ["myrm = myrm.main:main"]
	}
)
