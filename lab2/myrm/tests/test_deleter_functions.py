#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import unittest
import shutil

import myrm.errors as errors
import myrm.deleter as deleter

class TestDeleter(unittest.TestCase):
	TEST_DIR = 'TEST_DIR'
	PATH_OF_EMPTY_DIR = os.path.join(TEST_DIR, 'EMPTY_DIR')
	PATH_OF_NOT_EMPTY_DIR = os.path.join(TEST_DIR, 'NOT_EMPTY_DIR')
	PATH_OF_FILE = os.path.join(TEST_DIR, 'FILE')

	def setUp(self):
		os.makedirs(self.TEST_DIR)
		os.makedirs(self.PATH_OF_EMPTY_DIR)
		os.makedirs(os.path.join(self.PATH_OF_NOT_EMPTY_DIR, 'DIR1', 'DIR2'))
		open(self.PATH_OF_FILE, 'wb')

	def test_rmfile(self):
		mydeleter = deleter.Deleter(force=True)
		mydeleter.remove_file(self.PATH_OF_FILE)

		self.assertEqual(os.path.exists(self.PATH_OF_FILE), False)

	def test_rmdir(self):
		mydeleter = deleter.Deleter(force=True)
		
		with self.assertRaises(errors.IsDirectoryError):
			mydeleter.remove_dir(self.PATH_OF_EMPTY_DIR)

		self.assertEqual(os.path.exists(self.PATH_OF_EMPTY_DIR), True)
	
	def test_rmdir_emptyflag(self):
		mydeleter = deleter.Deleter(force=True, empty_dir=True)
		mydeleter.remove_dir(self.PATH_OF_EMPTY_DIR)

		self.assertEqual(os.path.exists(self.PATH_OF_EMPTY_DIR), False)

	def test_rmnotemptydir(self):
		mydeleter = deleter.Deleter(force=True)
		
		with self.assertRaises(errors.IsDirectoryError):
			mydeleter.remove_not_empty_dir(self.PATH_OF_NOT_EMPTY_DIR)

		self.assertEqual(os.path.exists(self.PATH_OF_NOT_EMPTY_DIR), True)

	def test_rmnotemptydir_recursively(self):
		mydeleter = deleter.Deleter(force=True, recursive=True)
		mydeleter.remove_not_empty_dir(self.PATH_OF_NOT_EMPTY_DIR)

		self.assertEqual(os.path.exists(self.PATH_OF_NOT_EMPTY_DIR), False)

	def test_rm(self):
		mydeleter = deleter.Deleter(force=True, recursive=True)
		mydeleter.remove(self.TEST_DIR)

		self.assertEqual(os.path.exists(self.TEST_DIR), False)

	def tearDown(self):
		if os.path.exists(self.TEST_DIR):
			shutil.rmtree(self.TEST_DIR)

if __name__ == '__main__':
	unittest.main()
