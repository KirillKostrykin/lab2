#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import unittest
import shutil

import myrm.deleter as deleter
import myrm.trash as trasher
import myrm.logger as logger
from logging import CRITICAL

class TestDeleter(unittest.TestCase):
	TEST_DIR = 'TEST_DIR'
	PATH_OF_DIR = os.path.join(TEST_DIR, 'EMPTY_DIR')
	TRASH = 'TEST_TRASH'

	def setUp(self):
		os.makedirs(self.PATH_OF_DIR)
		
		os.makedirs(os.path.join(self.TRASH, 'files'))
		os.makedirs(os.path.join(self.TRASH, 'info'))

		self.dir_inode = os.stat(self.PATH_OF_DIR).st_ino
		
	def test_remove_dir_totrash(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.move_to_trash(self.PATH_OF_DIR)

		path_dir_in_trash = os.path.join(self.TRASH, 'files/', str(self.dir_inode))
		self.assertEqual(os.path.exists(self.PATH_OF_DIR), False)
		self.assertEqual(os.path.exists(path_dir_in_trash), True)

	def test_restore_dir_with_inode(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.restore_file_with_inode(str(self.dir_inode))

		path_dir_in_trash = os.path.join(self.TRASH, 'files/', str(self.dir_inode))
		self.assertEqual(os.path.exists(self.PATH_OF_DIR), True)
		self.assertEqual(os.path.exists(path_dir_in_trash), False)

	def test_restore_dir_with_path(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.move_to_trash(self.PATH_OF_DIR)
		mytrasher.restore_file_with_path(self.PATH_OF_DIR)

		path_dir_in_trash = os.path.join(self.TRASH, 'files/', str(self.dir_inode))
		self.assertEqual(os.path.exists(self.PATH_OF_DIR), True)
		self.assertEqual(os.path.exists(path_dir_in_trash), False)

	def test_clear_trash(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		mydeleter = deleter.Deleter()

		dir_inode = os.stat(self.PATH_OF_DIR).st_ino
		path_dir_in_trash = os.path.join(self.TRASH, 'files/', str(dir_inode))
		
		mytrasher.move_to_trash(self.PATH_OF_DIR)
		mytrasher.clear(mydeleter)

		self.assertEqual(os.path.exists(path_dir_in_trash), False)


	def tearDown(self):
		if os.path.exists(self.TEST_DIR):
			shutil.rmtree(self.TEST_DIR)
		shutil.rmtree(self.TRASH)

if __name__ == '__main__':
	unittest.main()
