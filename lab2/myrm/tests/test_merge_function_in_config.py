#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import unittest
import shutil
import myrm.config as config

import myrm.deleter as deleter
import myrm.trash as trasher

class TestMerge(unittest.TestCase):
	def test_merge_of_params_in_one_length_dict(self):
		dict_one = {1: 1, 2: 2}
		dict_two = {1: 'one', 2: 'two'}

		self.assertEqual(config.merge_config_params_with_args(dict_one, dict_two) , {1: 'one', 2: 'two'})

	def test_merge_params_with_different_length(self):
		dict_one = {1: 1, 2: 2, 3: 3, 4: 4}
		dict_two = {1: 'one', 2: 'two'}

		self.assertEqual(config.merge_config_params_with_args(dict_one, dict_two) , {1: 'one', 2: 'two', 3: 3, 4: 4})
	
		dict_one = {}
		dict_two = {1: 'one', 2: 'two'}

		self.assertEqual(config.merge_config_params_with_args(dict_one, dict_two) , {})

if __name__ == '__main__':
	unittest.main()