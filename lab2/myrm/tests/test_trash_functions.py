#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
import os
import unittest
import shutil

import myrm.deleter as deleter
import myrm.trash as trasher
import myrm.logger as logger
from logging import CRITICAL

class TestDeleter(unittest.TestCase):
	TEST_DIR = 'TEST_DIR'
	PATH_OF_FILE1 = os.path.join(TEST_DIR, 'FILE1')
	PATH_OF_FILE2 = os.path.join(TEST_DIR, 'FILE2')
	PATH_OF_FILE3 = os.path.join(TEST_DIR, 'FILE3')
	TRASH = 'TEST_TRASH'

	def setUp(self):
		os.makedirs(self.TEST_DIR)
		
		os.makedirs(os.path.join(self.TRASH, 'files'))
		os.makedirs(os.path.join(self.TRASH, 'info'))
	
		open(self.PATH_OF_FILE1, 'wb')
		open(self.PATH_OF_FILE2, 'wb')
		open(self.PATH_OF_FILE3, 'wb')

		self.inode_file1 = os.stat(self.PATH_OF_FILE1).st_ino
		self.inode_file2 = os.stat(self.PATH_OF_FILE2).st_ino
		self.inode_file3 = os.stat(self.PATH_OF_FILE3).st_ino
		self.fileinode_list = [self.inode_file1, self.inode_file2, self.inode_file3]
		
	def test_remove_file_totrash(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.move_to_trash(self.PATH_OF_FILE1)

		path_file_in_trash = os.path.join(self.TRASH, 'files/', str(self.inode_file1))
		self.assertEqual(os.path.exists(self.PATH_OF_FILE1), False)
		self.assertEqual(os.path.exists(path_file_in_trash), True)

	def test_restore_file_with_inode(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.restore_file_with_inode(str(self.inode_file1))

		path_file_in_trash = os.path.join(self.TRASH, 'files/', str(self.inode_file1))
		self.assertEqual(os.path.exists(self.PATH_OF_FILE1), True)
		self.assertEqual(os.path.exists(path_file_in_trash), False)

	def test_restore_file_with_path(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		
		mytrasher.move_to_trash(self.PATH_OF_FILE1)
		mytrasher.restore_file_with_path(self.PATH_OF_FILE1)

		path_file_in_trash = os.path.join(self.TRASH, 'files/', str(self.inode_file1))
		self.assertEqual(os.path.exists(self.PATH_OF_FILE1), True)
		self.assertEqual(os.path.exists(path_file_in_trash), False)

	def test_clear_trash(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		mydeleter = deleter.Deleter()

		mytrasher.move_to_trash(self.PATH_OF_FILE1)
		mytrasher.clear(mydeleter)

		path_file_in_trash = os.path.join(self.TRASH, 'files/', str(self.inode_file1))
		self.assertEqual(os.path.exists(path_file_in_trash), False)

	def test_get_trash_elems(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		mydeleter = deleter.Deleter()

		mytrasher.move_to_trash(self.PATH_OF_FILE1)
		mytrasher.move_to_trash(self.PATH_OF_FILE2)
		mytrasher.move_to_trash(self.PATH_OF_FILE3)

		func_result = set()
		trash_file_list = mytrasher.get_trash_elems()
		for elem in trash_file_list:
			func_result.add(elem.inode)

		self.assertEqual(func_result, set(self.fileinode_list))

	def test_get_trash_size(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		mydeleter = deleter.Deleter()

		self.assertNotEqual(mytrasher.get_size(), os.path.getsize(self.TRASH))

	def test_flush(self):
		mylogger = logger.Logger(CRITICAL)
		mytrasher = trasher.Trash(force=True, totrash=True, trash_path=self.TRASH, logger_=mylogger)
		mydeleter = deleter.Deleter()

		mytrasher.move_to_trash(self.PATH_OF_FILE1)
		mytrasher.move_to_trash(self.PATH_OF_FILE2)
		mytrasher.move_to_trash(self.PATH_OF_FILE3)

		mytrasher.flush(self.fileinode_list, mydeleter)

		self.assertEqual(os.listdir(mytrasher.filedir_path), [])
		self.assertEqual(os.listdir(mytrasher.infodir_path), [])


	def tearDown(self):
		if os.path.exists(self.TEST_DIR):
			shutil.rmtree(self.TEST_DIR)
		shutil.rmtree(self.TRASH)

if __name__ == '__main__':
	unittest.main()
